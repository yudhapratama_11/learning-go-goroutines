package golang_goroutine

import (
	"fmt"
	"strconv"
	"testing"
)

func TestRangeChannel(t *testing.T) {
	channel := make(chan string)

	go func(channel chan string) {
		// harus di close disini kalo engga nanti di for range channel dibawah nunggu terus
		defer close(channel)
		for i := 0; i < 10; i++ {
			channel <- "Perulangan ke " + strconv.Itoa(i)
		}
	}(channel)

	// Ketika proses kirim channel sedang dijalankan, 
	// maka dapat dilakukan proses menerima secara concurrent
	for data := range channel {
		fmt.Println("Menerima data", data)
	}

}
