package golang_goroutine

import (
	"fmt"
	"testing"
	"time"
)

// func TestTicker(t *testing.T) {
// 	ticker := time.NewTicker(1 * time.Second)

// 	go func() {
// 		time.Sleep(5 * time.Second)
// 		ticker.Stop()
// 	}()

// 	for time := range ticker.C {
// 		fmt.Println(time)
// 	}
// }

func TestTick(t *testing.T) {
	channel := time.Tick(1 * time.Second) // tick ini untuk langsung mengambil datanya, kalo ticker berbentu object. dan ini gabisa di close

	for time := range channel {
		fmt.Println(time)
	}

}
