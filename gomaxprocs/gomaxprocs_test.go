package golang_goroutine

import (
	"fmt"
	"runtime"
	"sync"
	"testing"
	"time"
)

func TestGetGomaxprocs(t *testing.T) {
	group := sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		group.Add(1)
		go func() {
			time.Sleep(3 * time.Second)
			group.Done()
		}()
	}

	totalCpu := runtime.NumCPU() // Jumlah CPU
	fmt.Println("Total CPU", totalCpu)

	totalThread := runtime.GOMAXPROCS(-1) // Jumlah Thread
	fmt.Println("Total Thread", totalThread)

	totalGoroutine := runtime.NumGoroutine() // Jumlah Goroutine
	fmt.Println("Total Goroutine", totalGoroutine)

	group.Wait()
}

func TestChangeThreadNumber(t *testing.T) {
	group := sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		group.Add(1)
		go func() {
			time.Sleep(3 * time.Second)
			group.Done()
		}()
	}

	totalCpu := runtime.NumCPU() // Jumlah CPU
	fmt.Println("Total CPU", totalCpu)

	runtime.GOMAXPROCS(20)                // Mengubah Jumlah Thread
	totalThread := runtime.GOMAXPROCS(-1) // Jumlah Thread
	fmt.Println("Total Thread", totalThread)

	totalGoroutine := runtime.NumGoroutine() // Jumlah Goroutine
	fmt.Println("Total Goroutine", totalGoroutine)

	group.Wait()
}
