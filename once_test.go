package golang_goroutine

import (
	"fmt"
	"sync"
	"testing"
)

var counter = 0

func OnlyOnce() {
	counter++
}

func TestOnce(t *testing.T) {
	once := sync.Once{}
	group := sync.WaitGroup{}

	for i := 0; i < 10000; i++ {
		group.Add(1) // add dipanggil sebelum memanggil goroutine
		go func() {
			// OnlyOnce()
			once.Do(OnlyOnce) // hanya boleh dieksekusi 1x pada goroutine
			group.Done()
		}()
	}

	group.Wait()
	fmt.Println("Counter = ", counter)
}
