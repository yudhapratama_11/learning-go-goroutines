package golang_goroutine

import (
	"fmt"
	"testing"
)

func TestBufferedChannel(t *testing.T) {
	messages := make(chan int, 5)

	for i := 0; i < 5; i++ {
		fmt.Println("send data", i)
		messages <- i
	}

	go func() {
		for {
			i := <-messages
			fmt.Println("receive data", i)
		}
	}()

}
