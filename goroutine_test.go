package golang_goroutine

import (
	"fmt"
	"testing"
	"time"
)

func RunHelloWorld() {
	fmt.Println("Hello World")
}


func DisplayNumber(number int) {
	fmt.Println("Display", number)
}

func TestCreateGoroutine(t *testing.T) {
	go RunHelloWorld() // menjalankan secara asynchronous
	fmt.Println("Ups")

	time.Sleep(1 * time.Second)
}

func TestManyGoroutine(t *testing.T) {
	for i := 0; i < 100000; i++ {
		go DisplayNumber(i) // semacam async
	}

	time.Sleep(40 * time.Second)
}
