package golang_goroutine

import (
	"fmt"
	"testing"
	"time"
)

func GiveMeResponse(channel chan string) {
	time.Sleep(2 * time.Second)
	channel <- "Yudha Pratama"
}

func TestChannelAsParameter(t *testing.T) {
	channel := make(chan string)
	defer close(channel)

	go GiveMeResponse(channel) // Pengirim

	fmt.Printf("Current Unix Time Before Get Data: %v\n", time.Now().Unix())

	data := <-channel // ini ditunggu datanya channel sampe terisi (await)

	fmt.Printf("Current Unix Time After Get Data: %v\n", time.Now().Unix())
	fmt.Println(data)
}
