package golang_goroutine

import (
	"fmt"
	"testing"
	"time"
)

func TestInOutChannel(t *testing.T) {
	channel := make(chan string)
	go OnlyIn(channel)
	go OnlyOut(channel)

	time.Sleep(5 * time.Second)
}

// channel hanya untuk mengirimkan data, tidak bisa untuk mengambil data (in)
func OnlyIn(channel chan<- string) {
	time.Sleep(2 * time.Second)
	channel <- "Yudha Pratama"
}

// channel hanya untuk mengambil data (out)
func OnlyOut(channel <-chan string) {
	data := <-channel // menerima data
	fmt.Println(data)
}
