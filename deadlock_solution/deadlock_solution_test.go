package golang_goroutine

import (
	"fmt"
	"sync"
	"testing"
)

type UserBalance struct {
	Mutex   sync.RWMutex
	Name    string
	Balance int
}

func TestDeadlockSolution(t *testing.T) {
	user1 := UserBalance{
		Name:    "Budi",
		Balance: 1000000,
	}

	user2 := UserBalance{
		Name:    "Sandi",
		Balance: 1000000,
	}

	group := &sync.WaitGroup{}

	group.Add(1) // group.add wajib dipanggil dulu sebelum menambahkan goroutine dibawah
	go Transfer(&user1, &user2, 100000, group)
	group.Add(1) // group.add wajib dipanggil dulu sebelum menambahkan goroutine dibawah
	go Transfer(&user2, &user1, 200000, group)
	group.Wait()

	fmt.Println("User ", user1.Name, ", Balance ", user1.Balance)
	fmt.Println("User ", user2.Name, ", Balance ", user2.Balance)
}

func (user *UserBalance) Lock() {
	user.Mutex.Lock()
}

func (user *UserBalance) Unlock() {
	user.Mutex.Unlock()
}

func (user *UserBalance) Change(amount int) {
	user.Balance = user.Balance + amount
}

func Transfer(user1 *UserBalance, user2 *UserBalance, amount int, group *sync.WaitGroup) {
	defer group.Done()

	user1.Lock()
	fmt.Println("Lock user1", user1.Name)
	user1.Change(-amount)

	// time.Sleep(1 * time.Second) // kalau ada delay, masih ada kemungkinan deadlock

	user2.Lock()
	fmt.Println("Lock user2", user2.Name)
	user2.Change(amount)

	user1.Unlock()
	user2.Unlock()

	fmt.Println(amount)
}
