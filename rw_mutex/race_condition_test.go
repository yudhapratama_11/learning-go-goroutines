package golang_goroutine

// type BankAccount struct {
// 	RWMutex sync.RWMutex
// 	Balance int
// }

// 	time.Sleep(3 * time.Second)
// 	fmt.Println("Counter = ", x) //  harusnya jawabannya 1000 * 100 = 100000, tapi engga sampe karna x pas ditambah itu ada yang bisa sama.
// }

// func TestMutex(t *testing.T) {
// 	x := 0
// 	var mutex sync.Mutex
// 	for i := 1; i <= 1000; i++ {
// 		go func() {
// 			for j := 1; j <= 100; j++ {
// 				mutex.Lock() // diprioritaskan 1 goroutine saja yang bisa akses
// 				x += 1
// 				mutex.Unlock()
// 			}
// 		}()
// 	}

// 	time.Sleep(3 * time.Second)
// 	fmt.Println("Counter = ", x) // 100000 karna sudah lock
// }

// func (account *BankAccount) AddBalance(amount int) {
// 	account.RWMutex.Lock()
// 	account.Balance = account.Balance + 1
// 	account.RWMutex.Unlock()
// }

// func (account *BankAccount) GetBalance() int {
// 	account.RWMutex.RLock()
// 	balance := account.Balance
// 	account.RWMutex.RUnlock()
// 	return balance
// }

// func TestReadWriteMutex(t *testing.T) {
// 	account := BankAccount{}

// 	for i := 1; i <= 100; i++ {
// 		go func() {
// 			for j := 1; j <= 100; j++ {
// 				account.AddBalance(1)
// 				fmt.Println(account.GetBalance())
// 			}
// 		}()
// 	}

// 	time.Sleep(5 * time.Second)
// 	fmt.Println("Balance = ", account.Balance) // 100000 karna sudah lock
// }
