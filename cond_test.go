package golang_goroutine

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

var locker = sync.Mutex{}
var cond = sync.NewCond(&locker)
var group = sync.WaitGroup{}

func WaitCondition(value int) {
	defer group.Done()

	cond.L.Lock()
	cond.Wait() // menunggu process, harus ada signal untuk melanjutkan ke proses berikutnya
	fmt.Println("Done", value)
	cond.L.Unlock()
}

func TestCond(t *testing.T) {
	for i := 0; i < 10; i++ {
		group.Add(1)
		go WaitCondition(i)
	}

	// go func() {
	// 	for i := 0; i < 10; i++ {
	// 		// time.Sleep(1 * time.Second)
	// 		cond.Signal() // signal untuk memberi sinyal untuk melanjutkan go routine yang statusnya wait ke proses berikutnya
	// 	}
	// }()

	go func() {
		for i := 0; i < 10; i++ {
			time.Sleep(1 * time.Second)
			cond.Broadcast() // signal untuk memberi sinyal untuk melanjutkan semua go routine yang statusnya wait ke proses berikutnya
		}
	}()

	group.Wait()
}
