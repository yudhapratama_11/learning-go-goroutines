package golang_goroutine

import (
	"fmt"
	"testing"
)

func TestRaceCondition(t *testing.T) {
	x := 0
	for i := 1; i <= 1000; i++ {
		go func() {
			for j := 1; j <= 100; j++ {
				x += 1
			}
		}()
	}

	fmt.Println(x)
}
