package golang_goroutine

import (
	"fmt"
	"testing"
	"time"
)

func TestCreateChannel(t *testing.T) {
	channel := make(chan string)
	defer close(channel) //dieksekusi setelah semua perintah selesai (pasti dieksekusi)

	go func() {
		channel <- "New Channel"
	}() // harus pake go karna standard goroutine untuk set channel (async)

	data := <-channel

	fmt.Println(data)

}

// func TestChannel(t *testing.T) {
// 	channel := make(chan string)

// 	go func() {
// 		fmt.Printf("Current Unix Time 0: %v\n", time.Now().Unix())
// 		time.Sleep(2 * time.Second) // fungsi sleep ini adalah untuk delay
// 		fmt.Printf("Current Unix Time 1: %v\n", time.Now().Unix())
// 		channel <- "Yudha Pratama"
// 		fmt.Println("Selesai mengirim data ke channel")
// 		fmt.Printf("Current Unix Time 2: %v\n", time.Now().Unix())
// 	}()

// 	data := <-channel
// 	fmt.Println(data)
// 	fmt.Printf("Current Unix Time 3: %v\n", time.Now().Unix())
// 	time.Sleep(5 * time.Second)
// 	fmt.Printf("Current Unix Time 4: %v\n", time.Now().Unix())
// 	defer close(channel)
// }

func GiveMeResponse(channel chan string) {
	// fmt.Printf("Current Unix Time Get Response: %v\n", time.Now().Unix())
	time.Sleep(2 * time.Second)
	channel <- "Yudha Pratama"
}

// func TestChannelAsParameter(t *testing.T) {
// 	channel := make(chan string)
// 	// channel1 := make(chan string)

// 	go GiveMeResponse(channel, "Yudha Pratama") // Pengirim

// 	fmt.Printf("Current Unix Time Before Get Data: %v\n", time.Now().Unix())
// 	data := <-channel // ini ditunggu datanya channel sampe terisi (await)
// 	fmt.Printf("Current Unix Time After Get Data: %v\n", time.Now().Unix())
// 	fmt.Println(data)

// 	// fmt.Printf("Current Unix Time beforegetdata1: %v\n", time.Now().Unix())
// 	// data1 := <-channel1
// 	// fmt.Printf("Current Unix Time getdata1: %v\n", time.Now().Unix())
// 	// fmt.Println(data1)
// 	// fmt.Printf("Current Unix Time 2: %v\n", time.Now().Unix())

// 	time.Sleep(5 * time.Second)
// 	fmt.Printf("Current Unix Time End: %v\n", time.Now().Unix())
// 	defer close(channel)
// }


func TestSelectChannel(t *testing.T) {
	channel1 := make(chan string)
	channel2 := make(chan string)

	go GiveMeResponse(channel1)
	go GiveMeResponse(channel2)

	counter := 0
	for {
		select {
		case data := <-channel1:
			fmt.Println("Data dari Channel 1", data)
			counter++
		case data := <-channel2:
			fmt.Println("Data dari Channel 2", data)
			counter++
		}
		if counter == 2 {
			break
		}
	}
}

func TestDefaultSelectChannel(t *testing.T) {
	channel1 := make(chan string)
	channel2 := make(chan string)

	go GiveMeResponse(channel1)
	go GiveMeResponse(channel2)

	counter := 0
	for {
		select {
		case data := <-channel1:
			fmt.Println("Data dari Channel 1", data)
			counter++
		case data := <-channel2:
			fmt.Println("Data dari Channel 2", data)
			counter++
		default:
			fmt.Println("Menunggu data")
		}
		if counter == 2 {
			break
		}
	}
}
