package golang_goroutine

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

type BankAccount struct {
	RWMutex sync.RWMutex
	Balance int
}

func TestMutex(t *testing.T) {
	x := 0
	var mutex sync.Mutex
	for i := 1; i <= 1000; i++ {
		go func() {
			for j := 1; j <= 100; j++ {
				mutex.Lock() // diprioritaskan 1 goroutine saja yang bisa akses
				x += 1
				mutex.Unlock()
			}
		}()
	}

	time.Sleep(3 * time.Second)
	fmt.Println("Counter = ", x) // 100000 karna sudah lock
}
